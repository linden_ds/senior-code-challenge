package com.mindex.challenge;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.EmployeeService;
import com.mindex.challenge.service.ReportingStructureService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataBootstrapTest {

    @Autowired
    private DataBootstrap dataBootstrap;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ReportingStructureService reportingStructureService;

    @Before
    public void init() {
        dataBootstrap.init();
    }

    @Test
    public void test() {
        Employee employee = employeeRepository.findByEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f");
        assertNotNull(employee);
        assertEquals("John", employee.getFirstName());
        assertEquals("Lennon", employee.getLastName());
        assertEquals("Development Manager", employee.getPosition());
        assertEquals("Engineering", employee.getDepartment());
    }

    @Test
    public void testGenerateReportingStructure() {
        ReportingStructure reportingStructure
                = reportingStructureService.generateForEmployeeId("16a596ae-edd3-4847-99fe-c4518e82c86f");
        assertNotNull(reportingStructure);
        assertEquals(4, (int)reportingStructure.getNumberOfReports());
        Employee manager = reportingStructure.getEmployee();
        assertEquals("John", manager.getFirstName());
        assertEquals("Lennon", manager.getLastName());
        assertEquals("Development Manager", manager.getPosition());
        assertEquals("Engineering", manager.getDepartment());
        List<Employee> directReports = manager.getDirectReports();
        assertEquals(2, directReports.size());
        Employee report = directReports.get(0);
        assertEquals("Paul", report.getFirstName());
        assertEquals("McCartney", report.getLastName());
        assertEquals("Developer I", report.getPosition());
        assertEquals("Engineering", report.getDepartment());
        report = directReports.get(1);
        assertEquals("Ringo", report.getFirstName());
        assertEquals("Starr", report.getLastName());
        assertEquals("Developer V", report.getPosition());
        assertEquals("Engineering", report.getDepartment());
        directReports = report.getDirectReports();
        assertEquals(2, directReports.size());
        report = directReports.get(0);
        assertEquals("Pete", report.getFirstName());
        assertEquals("Best", report.getLastName());
        assertEquals("Developer II", report.getPosition());
        assertEquals("Engineering", report.getDepartment());
        report = directReports.get(1);
        assertEquals("George", report.getFirstName());
        assertEquals("Harrison", report.getLastName());
        assertEquals("Developer III", report.getPosition());
        assertEquals("Engineering", report.getDepartment());
    }
}
