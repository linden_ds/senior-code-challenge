package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.CompensationRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.CompensationService;
import com.mindex.challenge.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.UUID;

@Service
public class CompensationServiceImpl implements CompensationService {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CompensationRepository compensationRepository;

    @Override
    public Compensation create(Compensation compensation) {
        if (null == compensation.getEmployee() || StringUtils.isEmpty(compensation.getEmployee().getEmployeeId())) {
            return null;
        }
        compensation.setCompensationId(UUID.randomUUID().toString());
        return compensationRepository.insert(compensation);
    }

    @Override
    public Compensation readByEmployeeId(String id) {
        Employee query = new Employee();
        query.setEmployeeId(id);
        return compensationRepository.findByEmployeeEmployeeId(id);
    }
}
