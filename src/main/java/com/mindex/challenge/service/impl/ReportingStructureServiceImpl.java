package com.mindex.challenge.service.impl;

import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.ReportingStructure;
import com.mindex.challenge.service.EmployeeService;
import com.mindex.challenge.service.ReportingStructureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ReportingStructureServiceImpl implements ReportingStructureService {

    @Autowired
    private EmployeeService employeeService;

    private int buildReportsRecursively(Set<String> foundIds, Employee employee) {
        int numReports = 0;
        List<Employee> populatedReports = new ArrayList<>();
        if (null != employee.getDirectReports()) {
            for (Employee report : employee.getDirectReports()) {
                String reportId = report.getEmployeeId();
                if (!foundIds.contains(reportId)) {
                    foundIds.add(reportId);
                    numReports++;
                    Employee populated;
                    if (null == (populated = employeeService.read(reportId))) {
                        populatedReports.add(report);
                    } else {
                        numReports += buildReportsRecursively(foundIds, populated);
                        populatedReports.add(populated);
                    }
                }
            }
        }
        employee.setDirectReports(populatedReports);
        return numReports;
    }

    public int buildAndCountReports(Employee employee) {
        Set<String> idSet = new HashSet<>();
        idSet.add(employee.getEmployeeId());
        return buildReportsRecursively(idSet, employee);
    }

    @Override
    public ReportingStructure generateForEmployeeId(String employeeId) {
        Employee employee = employeeService.read(employeeId);
        ReportingStructure reportingStructure = null;
        if (null != employee) {
            reportingStructure = new ReportingStructure();
            reportingStructure.setEmployee(employee);
            reportingStructure.setNumberOfReports(buildAndCountReports(employee));
        }

        return reportingStructure;
    }
}
