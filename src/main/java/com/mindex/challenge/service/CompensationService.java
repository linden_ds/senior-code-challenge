package com.mindex.challenge.service;

import com.mindex.challenge.data.Compensation;

import java.util.Date;

public interface CompensationService {
    public Compensation create(Compensation compensation);
    public Compensation readByEmployeeId(String id);
}
